import { useEffect, useState } from "react";
import { AppContext } from "./AppContext";
import { CompaniesInfo } from "./components/CompaniesInfo";
import { UsersInfo } from "./components/UsersInfo";
import "./App.css";

const data = {
  companyInfo: {
    name: "qq",
    abn: "ww",
    tel: "ee",
    email: "rr",
    address: "tt",
    country: "yy",
    superb: "uu",
    postal: "ii",
  },
  employees: [
    {
      firstName: "aa",
      lastName: "ss",
      gender: "dd",
      dob: "ff",
      address: "gg",
      country: "hh",
      supperb: "jj",
      postal: "kk",
    },
    {
      firstName: "zz",
      lastName: "xx",
      gender: "cc",
      dob: "vv",
      address: "bb",
      country: "nn",
      supperb: "dd",
      postal: "ff",
    },
  ],
};

function App() {
  const [fetchedData, setFetchedData] = useState(null);
  const [error, setError] = useState(null);
  // fetch("https://8b5f-54-153-228-19.ngrok.io/", {'Access-Control-Allow-Origin': '*'}).then(data => console.log('data', data));
  const getData = () =>
    fetch("https://8b5f-54-153-228-19.ngrok.io/", {
      "Access-Control-Allow-Origin": "*",
    });
  // new Promise((res) => {
  //   setTimeout(() => {
  //     res(data);
  //   }, 1500);
  // });

  useEffect(() => {
    getData().then(setFetchedData).error(setError);
  }, []);

  return (
    <div className="App">
      {error && <p>{error}</p>}
      {fetchedData ? (
        <>
          <AppContext.Provider value={{ data: fetchedData }}>
            <CompaniesInfo />
            <UsersInfo />
          </AppContext.Provider>
        </>
      ) : (
        <div>LOADER</div>
      )}
    </div>
  );
}

export default App;
