import { useContext } from "react";
import { AppContext } from "../AppContext";

export const CompaniesInfo = () => {
  const { data } = useContext(AppContext);
  const { companyInfo } = data;
  return (
    <>
      <h2>Company Info</h2>
      <p>abn: {companyInfo.abn}</p>
      <p>address: {companyInfo.address}</p>
      <p>country: {companyInfo.country}</p>
      <p>email: {companyInfo.email}</p>
      <p>name: {companyInfo.name}</p>
      <p>postal: {companyInfo.postal}</p>
      <p>superb: {companyInfo.superb}</p>
      <p>tel: {companyInfo.tel}</p>
    </>
  );
};
