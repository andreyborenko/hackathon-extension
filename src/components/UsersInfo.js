import { useContext } from "react";
import { AppContext } from "../AppContext";

export const UserInfo = ({ userInfo }) => (
  <>
    <h2>User Info</h2>
    <p>address: {userInfo.address}</p>
    <p>country: {userInfo.country}</p>
    <p>dob: {userInfo.dob}</p>
    <p>firstName: {userInfo.firstName}</p>
    <p>gender: {userInfo.gender}</p>
    <p>lastName: {userInfo.lastName}</p>
    <p>postal: {userInfo.postal}</p>
    <p>supperb: {userInfo.supperb}</p>
  </>
);

export const UsersInfo = () => {
  const { data } = useContext(AppContext);
  const { employees } = data;
  return (
    <>
      {employees.map((employee, index) => (
        <UserInfo key={`${employee.firstName} ${index}`} userInfo={employee} />
      ))}
    </>
  );
};
